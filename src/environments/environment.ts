// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCYh47GNzJzJGKa5-cYkE8azEiSK9pNV94",
    authDomain: "moadb2.firebaseapp.com",
    databaseURL: "https://moadb2.firebaseio.com",
    projectId: "moadb2",
    storageBucket: "moadb2.appspot.com",
    messagingSenderId: "663084575528",
    appId: "1:663084575528:web:e4753a32251d42ea057e1d" }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
