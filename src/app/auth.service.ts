import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;}
  
    getUser(){
      return this.user
    }
  
    SignUp(email:string, password:string){
      this.afAuth
          .auth
          .createUserWithEmailAndPassword(email,password)
          .then(res => 
            {
              console.log('Succesful sign up',res);
              this.router.navigate(['/welcome']);
            }
          );
  
    }
  
    Logout(){
      this.afAuth.auth.signOut();  
    }
  
    login(email:string, password:string){
      this.afAuth
          .auth.signInWithEmailAndPassword(email,password)
          .then(
             res =>  
              {
                console.log('Succesful Login',res);
                this.router.navigate(['/welcome']);
              }     
          )
    }    
}
