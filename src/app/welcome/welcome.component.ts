import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService:AuthService) { }

  userId:string;
  useremail:string;

  ngOnInit() {

    this.authService.user.subscribe(
              user => {
                this.userId = user.uid;
                this.useremail = user.email;
                console.log(this.userId)
               }
            )
  }

}
